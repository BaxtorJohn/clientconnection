package com.model;

import java.io.Serializable;

public class Request implements Serializable {
	private static final long serialVersionUID = 1L;
	Header head;
	Body body;

	public Request() {
	}

	public Header getHead() {
		return head;
	}

	public void setHead(Header head) {
		this.head = head;
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "Request [head=" + head + ", body=" + body + "]";
	}

	public enum ResponseCode {
		CODE_SUCCESS, CODE_SERVER_ERROR, CODE_CLIENT_ERROR, CODE_QUIT
	}

}
