/*
 * 
 */
package com.model;

import java.io.Serializable;

/**
 * The Class Account.
 */
public class Account implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 477879574875781553L;

	/** The account id. */
	private int accountId;

	/** The code. */
	private String code;

	/** The name. */
	private String name;

	/** The password. */
	private String password;

	/** The budget. */
	private String budget;

	/** The status. */
	private String status;

	/**
	 * Instantiates a new account.
	 */
	public Account() {
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Account ["+" code=" + code + ", name=" + name + ", budget=" + formatMoneyToString(budget)
				+ ", status=" + status + "]";
	}
	public static String formatMoneyToString(String amount) {
		StringBuilder result = new StringBuilder();
		StringBuilder temporary = new StringBuilder();
		int character = 1;
		String naturalParts = amount;
		if(amount.contains(".")) {
			naturalParts = amount.substring(0, amount.length()-4);
		}
		for (int i = naturalParts.length() - 1; i >= 0; i--) {
			temporary.append(naturalParts.charAt(i));
			if (character == 3 && i != 0) {
				temporary.append(",");
				character = 0;
			}
			character++;
		}
		// revert character and save in result
		for (int i = temporary.length() - 1; i >= 0; i--) {
			result.append(temporary.charAt(i));
		}
		if(amount.contains(".")) {
			result.append(amount.substring(amount.length()-4));
		}
		return result.toString();
	}
	/**
	 * Gets the account id.
	 *
	 * @return the account id
	 */
	public int getAccountId() {
		return accountId;
	}

	/**
	 * Sets the account id.
	 *
	 * @param accountId the new account id
	 */
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
