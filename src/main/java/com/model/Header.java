package com.model;

import java.io.Serializable;

import com.model.Request.ResponseCode;

public class Header implements Serializable{
	private static final long serialVersionUID = 1L;
	private ResponseCode code;
	private int session;
	
	public Header() {}

	public Header(ResponseCode code, int session) {
		super();
		this.code = code;
		this.session = session;
	}

	public ResponseCode getCode() {
		return code;
	}

	public void setCode(ResponseCode code) {
		this.code = code;
	}

	public int getSession() {
		return session;
	}

	public void setSession(int session) {
		this.session = session;
	}

	@Override
	public String toString() {
		return "Header [code=" + code + ", session=" + session + "]";
	}
	
}
