package com.model;

import java.io.Serializable;

public class Body implements Serializable{
	private static final long serialVersionUID = 1L;
	private String message;
	private Account account;

	public Body(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "Body [message=" + message + "]";
	}
	
}
