/*
 * 
 */
package com.thach.ClientConnection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import org.apache.log4j.Logger;

import com.model.Body;
import com.model.Header;
import com.model.Request;
import com.model.Request.ResponseCode;

/**
 * read and write object must compliant about structure of Object, implement
 * serializable
 *
 */
public class App {
	private static final Logger LOG = Logger.getLogger(App.class);
	private static String line = "";

	public static ObjectOutputStream output;
	public static ObjectInputStream input;

	public static final void main(String[] args) {
		try {
			Socket clientSocket = new Socket("127.0.0.1", 12345);
			output = new ObjectOutputStream(clientSocket.getOutputStream());
			input = new ObjectInputStream(clientSocket.getInputStream());
			Scanner sc = new Scanner(System.in);
			while (true) {
				final Request response = (Request) input.readObject();
				// display message response
				LOG.info(response.getBody().getMessage());

				// quit program
				if (response.getHead().getCode().equals(ResponseCode.CODE_QUIT)) {
					sc.close();
					break;
				} else if (response.getHead().getCode().equals(ResponseCode.CODE_SERVER_ERROR)) {
					LOG.warn("Problem from server");
				} else if (response.getHead().getCode().equals(ResponseCode.CODE_CLIENT_ERROR)) {
					LOG.warn("Problem in client");
				}
				line = sc.nextLine();
				// quit program
				if (line.equalsIgnoreCase("quit")) {
					sc.close();
					break;
				}

				// received option from client, then display total current amount and announce
				// user input amount to withdraw or recharge
				// convert line to request object, send object
				String line2 = "";
				if(response.getBody().getAccount() != null) {
					LOG.info("Enter the number ...");
					line2 = sc.nextLine();
				}

				sendRequest(response.getHead().getSession(), line + "\n" + line2);
			}
			clientSocket.close();
		} catch (UnknownHostException e) {
			LOG.warn("Unknownhost connected " + e.getMessage());
		} catch (IOException e) {
			LOG.error(e.getMessage());
		} catch (ClassNotFoundException e) {
			LOG.error(e.getMessage());
		}
	}

	/**i
	 * Display amount. - Method is used display total mount and announce user input
	 * amount want to perform actions
	 *
	 * @param response the response
	 */
	private static void displayAmount(Request response) {
		//need check status of account
		String totalAmount = String.format("Your total current budget is %.3f VND", response.getBody().getAccount().getBudget() );
		LOG.info(totalAmount + "\nEnter your option");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
	}

	private static void sendQuitRequest(int session, ResponseCode codeQuit) {
		Request quitRequest = new Request();
		Header header = new Header();
		header.setCode(codeQuit);
		header.setSession(session);
		quitRequest.setHead(header);
		quitRequest.setBody(new Body(""));
		try {
			output.writeObject(quitRequest);
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
	}

	private static void sendRequest(int session, String line) {
		Request request = new Request();
		Header header = new Header();
		header.setSession(session);
		request.setHead(header);
		request.setBody(new Body(line));
		try {
			output.writeObject(request);
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
	}

}
